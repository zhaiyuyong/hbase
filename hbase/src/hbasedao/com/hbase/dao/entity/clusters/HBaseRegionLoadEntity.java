package com.hbase.dao.entity.clusters;

/**
 * HBase的regionServer的region的信息
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseRegionLoadEntity {
	/**
	 * region名
	 */
	private String name;
	/**
	 * region的列族数量
	 */
	private int stores;
	/**
	 * region的存储文件数量
	 */
	private int storeFiles;
	/**
	 * region的存储文件占用空间(单位为MB)
	 */
	private int storeFileSizeMB;
	/**
	 * region的存储文件的索引信息大小(单位为MB)
	 */
	private int storeFileIndexSizeMB;
	/**
	 * region使用的memStore的大小(单位为MB)
	 */
	private int memStoreSizeMB;
	/**
	 * region的本次统计周期内的TPS
	 */
	private long requestsCount;
	/**
	 * region的本次统计周期内的QPS
	 */
	private long readRequestsCount;
	/**
	 * region的本次统计周期内的WPS
	 */
	private long writeRequestsCount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStores() {
		return stores;
	}

	public void setStores(int stores) {
		this.stores = stores;
	}

	public int getStoreFiles() {
		return storeFiles;
	}

	public void setStoreFiles(int storeFiles) {
		this.storeFiles = storeFiles;
	}

	public int getStoreFileSizeMB() {
		return storeFileSizeMB;
	}

	public void setStoreFileSizeMB(int storeFileSizeMB) {
		this.storeFileSizeMB = storeFileSizeMB;
	}

	public int getStoreFileIndexSizeMB() {
		return storeFileIndexSizeMB;
	}

	public void setStoreFileIndexSizeMB(int storeFileIndexSizeMB) {
		this.storeFileIndexSizeMB = storeFileIndexSizeMB;
	}

	public int getMemStoreSizeMB() {
		return memStoreSizeMB;
	}

	public void setMemStoreSizeMB(int memStoreSizeMB) {
		this.memStoreSizeMB = memStoreSizeMB;
	}

	public long getRequestsCount() {
		return requestsCount;
	}

	public void setRequestsCount(long requestsCount) {
		this.requestsCount = requestsCount;
	}

	public long getReadRequestsCount() {
		return readRequestsCount;
	}

	public void setReadRequestsCount(long readRequestsCount) {
		this.readRequestsCount = readRequestsCount;
	}

	public long getWriteRequestsCount() {
		return writeRequestsCount;
	}

	public void setWriteRequestsCount(long writeRequestsCount) {
		this.writeRequestsCount = writeRequestsCount;
	}

	@Override
	public String toString() {
		return "HBaseRegionLoadEntity [name=" + name + ", stores=" + stores
				+ ", storeFiles=" + storeFiles + ", storeFileSizeMB="
				+ storeFileSizeMB + ", storeFileIndexSizeMB="
				+ storeFileIndexSizeMB + ", memStoreSizeMB=" + memStoreSizeMB
				+ ", requestsCount=" + requestsCount + ", readRequestsCount="
				+ readRequestsCount + ", writeRequestsCount="
				+ writeRequestsCount + "]";
	}

}