package com.hbase.dao.entity.clusters;

import java.util.Map;

/**
 * HBase的regionServer的信息
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseHServerLoadEntity {
	/**
	 * 服务器的域名
	 */
	private String hostName;
	/**
	 * 域名与RPC端口
	 */
	private String hostAndPort;
	/**
	 * 服务器的启动时间
	 */
	private long startTime;
	/**
	 * 服务器名
	 */
	private String serverName;
	/**
	 * 服务器的RPC端口
	 */
	private int port;
	/**
	 * 当前region服务器上线的region数量
	 */
	private int numberOfRegions;
	/**
	 * 统计所有的API请求
	 */
	private int numberOfRequests;
	/**
	 * JVM已使用的内存(单位为MB)
	 */
	private int usedHeapMB;
	/**
	 * JVM最大可使用的内存(单位为MB)
	 */
	private int maxHeapMB;
	/**
	 * 当前region服务器的储存文件数量(包括管理的所有region)
	 */
	private int storeFiles;
	/**
	 * 当前region服务器的储存文件的总存储量(单位为MB)
	 */
	private int storeFileSizeInMB;
	/**
	 * 当前region服务器的储存文件的索引大小(单位为MB)
	 */
	private int storeFileIndexSizeInMB;
	/**
	 * 当前region服务器的已使用写缓存的大小
	 */
	private int memstoreSizeInMB;
	/**
	 * 当前region服务器中每个region的负载情况
	 */
	private Map<String, HBaseRegionLoadEntity> hBaseRegionLoadEntityMap;

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostAndPort() {
		return hostAndPort;
	}

	public void setHostAndPort(String hostAndPort) {
		this.hostAndPort = hostAndPort;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getNumberOfRegions() {
		return numberOfRegions;
	}

	public void setNumberOfRegions(int numberOfRegions) {
		this.numberOfRegions = numberOfRegions;
	}

	public int getNumberOfRequests() {
		return numberOfRequests;
	}

	public void setNumberOfRequests(int numberOfRequests) {
		this.numberOfRequests = numberOfRequests;
	}

	public int getUsedHeapMB() {
		return usedHeapMB;
	}

	public void setUsedHeapMB(int usedHeapMB) {
		this.usedHeapMB = usedHeapMB;
	}

	public int getMaxHeapMB() {
		return maxHeapMB;
	}

	public void setMaxHeapMB(int maxHeapMB) {
		this.maxHeapMB = maxHeapMB;
	}

	public int getStoreFiles() {
		return storeFiles;
	}

	public void setStoreFiles(int storeFiles) {
		this.storeFiles = storeFiles;
	}

	public int getStoreFileSizeInMB() {
		return storeFileSizeInMB;
	}

	public void setStoreFileSizeInMB(int storeFileSizeInMB) {
		this.storeFileSizeInMB = storeFileSizeInMB;
	}

	public int getStoreFileIndexSizeInMB() {
		return storeFileIndexSizeInMB;
	}

	public void setStoreFileIndexSizeInMB(int storeFileIndexSizeInMB) {
		this.storeFileIndexSizeInMB = storeFileIndexSizeInMB;
	}

	public int getMemstoreSizeInMB() {
		return memstoreSizeInMB;
	}

	public void setMemstoreSizeInMB(int memstoreSizeInMB) {
		this.memstoreSizeInMB = memstoreSizeInMB;
	}

	public Map<String, HBaseRegionLoadEntity> gethBaseRegionLoadEntityMap() {
		return hBaseRegionLoadEntityMap;
	}

	public void sethBaseRegionLoadEntityMap(
			Map<String, HBaseRegionLoadEntity> hBaseRegionLoadEntityMap) {
		this.hBaseRegionLoadEntityMap = hBaseRegionLoadEntityMap;
	}

	@Override
	public String toString() {
		return "HBaseHServerLoadEntity [hostName=" + hostName
				+ ", hostAndPort=" + hostAndPort + ", startTime=" + startTime
				+ ", serverName=" + serverName + ", port=" + port
				+ ", numberOfRegions=" + numberOfRegions
				+ ", numberOfRequests=" + numberOfRequests + ", usedHeapMB="
				+ usedHeapMB + ", maxHeapMB=" + maxHeapMB + ", storeFiles="
				+ storeFiles + ", storeFileSizeInMB=" + storeFileSizeInMB
				+ ", storeFileIndexSizeInMB=" + storeFileIndexSizeInMB
				+ ", memstoreSizeInMB=" + memstoreSizeInMB
				+ ", hBaseRegionLoadEntityMap=" + hBaseRegionLoadEntityMap
				+ "]";
	}

}