package com.hbase.dao.entity.clusters;

import java.util.Collection;
import java.util.Map;

import org.apache.hadoop.hbase.ServerName;
import org.apache.hadoop.hbase.master.AssignmentManager.RegionState;

/**
 * HBase集群状态信息
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseClustersStatusEntity {
	/**
	 * 当前活着的region服务器的数量
	 */
	private int serversSize;
	/**
	 * 当前活着的region服务器的列表
	 */
	private Collection<ServerName> serversCollection;
	/**
	 * 当前处于不可用的region服务器的数量
	 */
	private int deadServers;
	/**
	 * 当前处于不可用的region服务器的列表
	 */
	private Collection<ServerName> deadServersCollection;
	/**
	 * 平均每台region服务器上线了多少个region
	 */
	private double averageLoad;
	/**
	 * 集群中region的总数量
	 */
	private int regionsCount;
	/**
	 * 集群的请求TPS
	 */
	private int requestsCount;
	/**
	 * 当前集群的软件编译版本
	 */
	private String hBaseVersion;
	/**
	 * 集群的唯一标识
	 */
	private String clusterId;
	/**
	 * 当前集群正在进行处理的region的事务列表
	 */
	private Map<String, RegionState> regionInTransitionMap;
	/**
	 * region服务器的当前负载情况
	 */
	private Map<String, HBaseHServerLoadEntity> hBaseHServerLoadEntityMap;

	public int getServersSize() {
		return serversSize;
	}

	public void setServersSize(int serversSize) {
		this.serversSize = serversSize;
	}

	public Collection<ServerName> getServersCollection() {
		return serversCollection;
	}

	public void setServersCollection(Collection<ServerName> serversCollection) {
		this.serversCollection = serversCollection;
	}

	public int getDeadServers() {
		return deadServers;
	}

	public void setDeadServers(int deadServers) {
		this.deadServers = deadServers;
	}

	public Collection<ServerName> getDeadServersCollection() {
		return deadServersCollection;
	}

	public void setDeadServersCollection(
			Collection<ServerName> deadServersCollection) {
		this.deadServersCollection = deadServersCollection;
	}

	public double getAverageLoad() {
		return averageLoad;
	}

	public void setAverageLoad(double averageLoad) {
		this.averageLoad = averageLoad;
	}

	public int getRegionsCount() {
		return regionsCount;
	}

	public void setRegionsCount(int regionsCount) {
		this.regionsCount = regionsCount;
	}

	public int getRequestsCount() {
		return requestsCount;
	}

	public void setRequestsCount(int requestsCount) {
		this.requestsCount = requestsCount;
	}

	public String gethBaseVersion() {
		return hBaseVersion;
	}

	public void sethBaseVersion(String hBaseVersion) {
		this.hBaseVersion = hBaseVersion;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public Map<String, RegionState> getRegionInTransitionMap() {
		return regionInTransitionMap;
	}

	public void setRegionInTransitionMap(
			Map<String, RegionState> regionInTransitionMap) {
		this.regionInTransitionMap = regionInTransitionMap;
	}

	public Map<String, HBaseHServerLoadEntity> gethBaseHServerLoadEntityMap() {
		return hBaseHServerLoadEntityMap;
	}

	public void sethBaseHServerLoadEntityMap(
			Map<String, HBaseHServerLoadEntity> hBaseHServerLoadEntityMap) {
		this.hBaseHServerLoadEntityMap = hBaseHServerLoadEntityMap;
	}

	@Override
	public String toString() {
		return "HBaseClustersStatusEntity [serversSize=" + serversSize
				+ ", serversCollection=" + serversCollection + ", deadServers="
				+ deadServers + ", deadServersCollection="
				+ deadServersCollection + ", averageLoad=" + averageLoad
				+ ", regionsCount=" + regionsCount + ", requestsCount="
				+ requestsCount + ", hBaseVersion=" + hBaseVersion
				+ ", clusterId=" + clusterId + ", regionInTransitionMap="
				+ regionInTransitionMap + ", hBaseHServerLoadEntityMap="
				+ hBaseHServerLoadEntityMap + "]";
	}

}