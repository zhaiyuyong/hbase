package com.hbase.dao.entity;

import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hbase.dao.util.CheckUtil;

/**
 * HBaseQueryEntity
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseQueryEntity {
	/**
	 * 模糊匹配主键的前缀
	 */
	private String prefixRowKey;
	/**
	 * 开始查询的主键
	 */
	private String startRowKey;
	/**
	 * 结束查询的主键
	 */
	private String stopRowKey;
	/**
	 * 查询的版本数
	 */
	private int maxVersions;
	/**
	 * 是否缓存Get、Scan的查询结果
	 */
	private boolean isCache = false;
	/**
	 * hbase.client.scanner.caching配置项可以设置HBase scanner一次从服务端抓取的数据条数 默认情况下一次一条
	 * 通过将其设置成一个合理的值 可以减少scan过程中next()的时间开销 代价是scanner需要通过客户端的内存来维持这些被cache的行记录
	 * 有三个地方可以进行配置 1)在HBase的配置文件中进行配置 2)通过调用HTable.setScannerCaching(int
	 * scannerCaching)进行配置 3)通过调用Scan.setCaching(int caching)进行配置 三者的优先级越来越高
	 */
	private int cacheSize;
	/**
	 * 过滤开始的时间戳
	 */
	private long minStamp;
	/**
	 * 过滤结束的时间戳
	 */
	private long maxStamp;
	/**
	 * 查询与此时间戳版本相同的数据(与minStamp maxStamp不能同时使用)
	 */
	private long timeStamp;
	/**
	 * 过滤的列族集合
	 */
	private List<String> familyList;
	/**
	 * 过滤的列集合(与familyList不能同时使用 2个同时存在则优先使用columnMap)
	 */
	private Map<String, List<String>> columnMap;
	/**
	 * 过滤器集合
	 */
	private FilterList filterList;
	/**
	 * 查询返回的最大行数
	 */
	private int maxRows;

	/**
	 * 设置Get查询的主键
	 * 
	 * @param RowKey
	 *            主键
	 */
	public void setRowKey(String RowKey) {
		this.startRowKey = RowKey;
	}

	/**
	 * 添加过滤的列族
	 * 
	 * @param family
	 *            列族名
	 */
	public void addFamily(String family) {
		CheckUtil.checkEmptyString(family);
		if (CheckUtil.isNull(familyList)) {
			familyList = Lists.newArrayList();
		}
		familyList.add(family);
	}

	/**
	 * 添加过滤的列
	 * 
	 * @param family
	 *            列族名
	 * @param column
	 *            列名
	 */
	public void addColumn(String family, String column) {
		CheckUtil.checkEmptyString(family);
		CheckUtil.checkEmptyString(column);
		if (CheckUtil.isNull(columnMap)) {
			columnMap = Maps.newHashMap();
		}
		List<String> columnList = columnMap.get(family);
		if (CheckUtil.isNull(columnList)) {
			columnList = Lists.newArrayList();
			columnList.add(column);
			columnMap.put(family, columnList);
		} else {
			columnList.add(column);
		}
	}

	/**
	 * 设置多个Filter之间的关系
	 * 
	 * @param Operator
	 *            MUST_PASS_ALL或MUST_PASS_ONE
	 */
	public void setFilterOperator(Operator Operator) {
		filterList = new FilterList(Operator);
	}

	/**
	 * 添加Filter
	 * 
	 * @param filter
	 */
	public void addFilter(Filter filter) {
		if (CheckUtil.isNull(filterList)) {
			filterList = new FilterList(Operator.MUST_PASS_ALL);
		}
		filterList.addFilter(filter);
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public String getPrefixRowKey() {
		return prefixRowKey;
	}

	public void setPrefixRowKey(String prefixRowKey) {
		this.prefixRowKey = prefixRowKey;
	}

	public String getStartRowKey() {
		return startRowKey;
	}

	public void setStartRowKey(String startRowKey) {
		this.startRowKey = startRowKey;
	}

	public String getStopRowKey() {
		return stopRowKey;
	}

	public void setStopRowKey(String stopRowKey) {
		this.stopRowKey = stopRowKey;
	}

	public int getMaxVersions() {
		return maxVersions;
	}

	public void setMaxVersions(int maxVersions) {
		this.maxVersions = maxVersions;
	}

	public boolean isCache() {
		return isCache;
	}

	public void setCache(boolean isCache) {
		this.isCache = isCache;
	}

	public int getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}

	public long getMinStamp() {
		return minStamp;
	}

	public void setMinStamp(long minStamp) {
		this.minStamp = minStamp;
	}

	public long getMaxStamp() {
		return maxStamp;
	}

	public void setMaxStamp(long maxStamp) {
		this.maxStamp = maxStamp;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public List<String> getFamilyList() {
		return familyList;
	}

	public void setFamilyList(List<String> familyList) {
		this.familyList = familyList;
	}

	public Map<String, List<String>> getColumnMap() {
		return columnMap;
	}

	public void setColumnMap(Map<String, List<String>> columnMap) {
		this.columnMap = columnMap;
	}

	public FilterList getFilterList() {
		return filterList;
	}

	public void setFilterList(FilterList filterList) {
		this.filterList = filterList;
	}

	@Override
	public String toString() {
		return "HBaseQueryEntity [prefixRowKey=" + prefixRowKey
				+ ", startRowKey=" + startRowKey + ", stopRowKey=" + stopRowKey
				+ ", maxVersions=" + maxVersions + ", isCache=" + isCache
				+ ", cacheSize=" + cacheSize + ", minStamp=" + minStamp
				+ ", maxStamp=" + maxStamp + ", timeStamp=" + timeStamp
				+ ", familyList=" + familyList + ", columnMap=" + columnMap
				+ ", filterList=" + filterList + ", maxRows=" + maxRows + "]";
	}

}