package com.hbase.dao.entity;

import java.util.Map;

import com.google.common.collect.Maps;
import com.hbase.dao.util.CheckUtil;

/**
 * HBase行记录(可保留多个版本)
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseRecordEntity {
	/**
	 * 主键
	 */
	private String rowKey;
	/**
	 * 列族Map容器(列族名-->列名-->时间戳 列值)
	 */
	private Map<String, Map<String, Map<Long, String>>> familyMap;

	public HBaseRecordEntity() {
		this.familyMap = Maps.newHashMap();
	}

	public HBaseRecordEntity(String rowKey) {
		this();
		this.rowKey = rowKey;
	}

	public String getRowKey() {
		return rowKey;
	}

	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}

	public Map<String, Map<String, Map<Long, String>>> getFamilyMap() {
		return familyMap;
	}

	public void setFamilyMap(
			Map<String, Map<String, Map<Long, String>>> familyMap) {
		this.familyMap = familyMap;
	}

	/**
	 * 添加数据(时间戳由服务器端指定)
	 * 
	 * @param family
	 *            列族名
	 * @param column
	 *            列名
	 * @param value
	 *            列值
	 */
	public void addColumn(String family, String column, String value) {
		CheckUtil.checkEmptyString(family);
		CheckUtil.checkEmptyString(column);
		CheckUtil.checkEmptyString(value);
		Map<String, Map<Long, String>> columnMap = familyMap.get(family);
		if (CheckUtil.isNull(columnMap)) {
			columnMap = Maps.newHashMap();
			Map<Long, String> valueMap = Maps.newHashMap();
			valueMap.put(0L, value);
			columnMap.put(column, valueMap);
			familyMap.put(family, columnMap);
		} else {
			Map<Long, String> valueMap = columnMap.get(column);
			if (CheckUtil.isNull(valueMap)) {
				valueMap = Maps.newHashMap();
				valueMap.put(0L, value);
				columnMap.put(column, valueMap);
			} else {
				valueMap.put(0L, value);
			}
		}
	}

	/**
	 * 添加数据(时间戳由客户端端指定)
	 * 
	 * @param family
	 *            列族名
	 * @param column
	 *            列名
	 * @param timeStamp
	 *            时间戳
	 * @param value
	 *            列值
	 * @param flag
	 *            true只保留最新版 false可以保留多个版本
	 */
	public void addColumn(String family, String column, long timeStamp,
			String value, boolean flag) {
		CheckUtil.checkEmptyString(family);
		CheckUtil.checkEmptyString(column);
		CheckUtil.checkEmptyString(value);
		CheckUtil.checkZero(timeStamp);
		Map<String, Map<Long, String>> columnMap = familyMap.get(family);
		if (CheckUtil.isNull(columnMap)) {
			columnMap = Maps.newHashMap();
			Map<Long, String> valueMap = Maps.newHashMap();
			valueMap.put(timeStamp, value);
			columnMap.put(column, valueMap);
			familyMap.put(family, columnMap);
		} else {
			Map<Long, String> valueMap = columnMap.get(column);
			if (CheckUtil.isNull(valueMap)) {
				valueMap = Maps.newHashMap();
				valueMap.put(timeStamp, value);
				columnMap.put(column, valueMap);
			} else {
				if (flag) {
					for (long ts : valueMap.keySet()) {
						if (timeStamp > ts) {
							// 防止添加多个版本(true:表示只需要一个最新的版本)
							valueMap.clear();
							valueMap.put(timeStamp, value);
						}
					}
				} else {
					valueMap.put(timeStamp, value);
				}
			}
		}
	}

	/**
	 * 只保留最新版本的数据(时间戳由客户端端指定)
	 * 
	 * @param family
	 *            列族名
	 * @param column
	 *            列名
	 * @param timeStamp
	 *            时间戳
	 * @param value
	 *            列值
	 */
	public void addColumn(String family, String column, long timeStamp,
			String value) {
		addColumn(family, column, timeStamp, value, true);
	}

	@Override
	public String toString() {
		return "HBaseRecordEntity [rowKey=" + rowKey + ", familyMap="
				+ familyMap + "]";
	}

}