package com.hbase.dao.entity.htable;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;

import com.google.common.collect.Lists;
import com.hbase.dao.util.CheckUtil;

/**
 * HBase表信息描述
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseTableDescribesEntity {
	/**
	 * 表名
	 */
	private String tableName;
	/**
	 * 列族集合
	 */
	private Map<String, HBaseTableFamilyDescribesEntity> familyMap;
	/**
	 * 最大的region的size(一个列族的存储单元的大小)
	 */
	private long maxFileSize;
	/**
	 * memStore的flush到HDFS上的文件大小
	 */
	private long memstoreFlushSize;
	/**
	 * 开始的主键
	 */
	private String startRowKey;
	/**
	 * 结束的主键
	 */
	private String endRowKey;
	/**
	 * 预分region的数量
	 */
	private int regionNumber;
	/**
	 * 预分region的主键集合
	 */
	private List<String> splitRowKeyList;
	/**
	 * 表是否只读(默认为false)
	 */
	private boolean isReadOnly = false;
	/**
	 * 是否延时日志刷写(默认为false)
	 */
	private boolean isDeferredLogFlush = false;

	public void addSplitRowKey(String rowKey) {
		if (StringUtils.isEmpty(rowKey)) {
			return;
		}
		if (CheckUtil.isNull(splitRowKeyList)) {
			splitRowKeyList = Lists.newArrayList();
		}
		splitRowKeyList.add(rowKey);
	}

	public byte[][] getSplitRowKeys() {
		if (CheckUtil.isEmpty(splitRowKeyList)) {
			return null;
		}
		byte[][] splits = new byte[splitRowKeyList.size()][];
		int length = splits.length;
		for (int i = 0; i < length; ++i) {
			splits[i] = Bytes.toBytes(splitRowKeyList.get(i));
		}
		return splits;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Map<String, HBaseTableFamilyDescribesEntity> getFamilyMap() {
		return familyMap;
	}

	public void setFamilyMap(
			Map<String, HBaseTableFamilyDescribesEntity> familyMap) {
		this.familyMap = familyMap;
	}

	public long getMaxFileSize() {
		return maxFileSize;
	}

	public void setMaxFileSize(long maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	public long getMemstoreFlushSize() {
		return memstoreFlushSize;
	}

	public void setMemstoreFlushSize(long memstoreFlushSize) {
		this.memstoreFlushSize = memstoreFlushSize;
	}

	public String getStartRowKey() {
		return startRowKey;
	}

	public void setStartRowKey(String startRowKey) {
		this.startRowKey = startRowKey;
	}

	public String getEndRowKey() {
		return endRowKey;
	}

	public void setEndRowKey(String endRowKey) {
		this.endRowKey = endRowKey;
	}

	public int getRegionNumber() {
		return regionNumber;
	}

	public void setRegionNumber(int regionNumber) {
		this.regionNumber = regionNumber;
	}

	public boolean isReadOnly() {
		return isReadOnly;
	}

	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	public boolean isDeferredLogFlush() {
		return isDeferredLogFlush;
	}

	public void setDeferredLogFlush(boolean isDeferredLogFlush) {
		this.isDeferredLogFlush = isDeferredLogFlush;
	}

	public List<String> getSplitRowKeyList() {
		return splitRowKeyList;
	}

	public void setSplitRowKeyList(List<String> splitRowKeyList) {
		this.splitRowKeyList = splitRowKeyList;
	}

	@Override
	public String toString() {
		return "HBaseTableDescribesEntity [tableName=" + tableName
				+ ", familyMap=" + familyMap + ", maxFileSize=" + maxFileSize
				+ ", memstoreFlushSize=" + memstoreFlushSize + ", startRowKey="
				+ startRowKey + ", endRowKey=" + endRowKey + ", regionNumber="
				+ regionNumber + ", splitRowKeyList=" + splitRowKeyList
				+ ", isReadOnly=" + isReadOnly + ", isDeferredLogFlush="
				+ isDeferredLogFlush + "]";
	}

}