package com.hbase.dao.entity.htable;

import org.apache.hadoop.hbase.io.hfile.Compression.Algorithm;
import org.apache.hadoop.hbase.regionserver.StoreFile.BloomType;

/**
 * HBase列族信息
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseTableFamilyDescribesEntity {
	/**
	 * 最大的TTL(单位是秒 过期数据会被自动删除)
	 */
	private int timeToLive;
	/**
	 * 是否使用BloomFilter(可提高随机查询效率 默认关闭)
	 */
	private BloomType bloomType;
	/**
	 * 数据压缩类型(默认无压缩)
	 */
	private Algorithm compression;
	/**
	 * 合并压缩类型(默认无压缩)
	 */
	private Algorithm compactionCompression;
	/**
	 * 数据最大保存的版本个数(默认为3)
	 */
	private int maxVersions;
	/**
	 * 是否把列族放进缓冲(默认为false)
	 */
	private boolean inMemory = false;
	/**
	 * HBase在一次读取过程中顺序读取多少数据到内存缓冲区(默认为64K)
	 */
	private int blockSize;
	/**
	 * 是否块缓存(默认为true)
	 */
	private boolean isBlockCacheEnabled = true;

	public int getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(int timeToLive) {
		this.timeToLive = timeToLive;
	}

	public BloomType getBloomType() {
		return bloomType;
	}

	public void setBloomType(BloomType bloomType) {
		this.bloomType = bloomType;
	}

	public int getMaxVersions() {
		return maxVersions;
	}

	public void setMaxVersions(int maxVersions) {
		this.maxVersions = maxVersions;
	}

	public boolean isInMemory() {
		return inMemory;
	}

	public void setInMemory(boolean inMemory) {
		this.inMemory = inMemory;
	}

	public Algorithm getCompression() {
		return compression;
	}

	public void setCompression(Algorithm compression) {
		this.compression = compression;
	}

	public Algorithm getCompactionCompression() {
		return compactionCompression;
	}

	public void setCompactionCompression(Algorithm compactionCompression) {
		this.compactionCompression = compactionCompression;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public boolean isBlockCacheEnabled() {
		return isBlockCacheEnabled;
	}

	public void setBlockCacheEnabled(boolean isBlockCacheEnabled) {
		this.isBlockCacheEnabled = isBlockCacheEnabled;
	}

	@Override
	public String toString() {
		return "HBaseTableFamilyDescribesEntity [timeToLive=" + timeToLive
				+ ", bloomType=" + bloomType + ", compression=" + compression
				+ ", compactionCompression=" + compactionCompression
				+ ", maxVersions=" + maxVersions + ", inMemory=" + inMemory
				+ ", blockSize=" + blockSize + ", isBlockCacheEnabled="
				+ isBlockCacheEnabled + "]";
	}

}