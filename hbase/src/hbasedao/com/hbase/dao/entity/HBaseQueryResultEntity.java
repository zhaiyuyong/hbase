package com.hbase.dao.entity;

import java.util.List;

import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import com.google.common.collect.Lists;
import com.hbase.dao.util.CheckUtil;

/**
 * 查询结果
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseQueryResultEntity {
	/**
	 * 查询到的总数
	 */
	private int totalCount;
	/**
	 * 结果集
	 */
	private List<Result> resultList;

	/**
	 * 提取数据转化为HBaseRecordEntity集合
	 * 
	 * @param flag
	 *            true:只提取最新版,false:可以提取多个版本
	 * @return List<HBaseRecordEntity>
	 */
	public List<HBaseRecordEntity> getRecordList(boolean flag) {
		List<HBaseRecordEntity> hBaseRecordEntityList = Lists.newArrayList();
		HBaseRecordEntity hBaseRecordEntity = null;
		List<Result> resultList = getResultList();
		if (CheckUtil.isEmpty(resultList)) {
			return null;
		}
		for (Result result : resultList) {
			if (result.isEmpty()) {
				continue;
			}
			hBaseRecordEntity = new HBaseRecordEntity(Bytes.toString(result
					.getRow()));
			for (KeyValue keyValue : result.raw()) {
				hBaseRecordEntity.addColumn(
						Bytes.toString(keyValue.getFamily()),
						Bytes.toString(keyValue.getQualifier()),
						keyValue.getTimestamp(),
						Bytes.toString(keyValue.getValue()), flag);
			}
			hBaseRecordEntityList.add(hBaseRecordEntity);
		}
		return hBaseRecordEntityList;
	}

	/**
	 * 提取数据转化为HBaseRecordEntity集合(只提取最新版)
	 * 
	 * @return List<HBaseRecordEntity>
	 */
	public List<HBaseRecordEntity> getRecordList() {
		return getRecordList(true);
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<Result> getResultList() {
		return resultList;
	}

	public void setResultList(List<Result> resultList) {
		this.resultList = resultList;
	}

	@Override
	public String toString() {
		return "HBaseQueryResultEntity [totalCount=" + totalCount
				+ ", resultList=" + resultList + "]";
	}

}