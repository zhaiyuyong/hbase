package com.hbase.dao.constant;

/**
 * HBase常量定义
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseConstant {
	/**
	 * 操作HBase的标识(默认是default)通过标识可以获得创建的Configuration HTablePool HTable
	 */
	public static final String HBASE_DEFAULT_INSTANCE = "default";
	/**
	 * zooKeeper集群IP地址(默认)
	 */
	public static final String HBASE_ZOOKEEPER_QUORUM = "127.0.0.1";
	/**
	 * zooKeeper集群端口号(默认)
	 */
	public static final String HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT = "2181";
	/**
	 * 每隔多少秒测试与服务器的连接(单位为毫秒)
	 */
	public static final String IPC_PING_INTERVAL = "3000";
	/**
	 * 是否开启无延迟
	 */
	public static final boolean HBASE_IPC_CLIENT_TCPNODELAY = true;
	/**
	 * 客户端被阻塞或者失败后重试间隔(单位为毫秒)
	 */
	public static final String HBASE_CLIENT_PAUSE = "200";
	/**
	 * 最大重试次数
	 */
	public static final String HBASE_CLIENT_RETRIES_NUMBER = "5";
	/**
	 * HTablePool已过时 当HTablePool中空闲的HTable数量达到HTABLE_POOL_MAX_SIZE则直接释放HTable
	 */
	public static final int HTABLE_POOL_MAX_SIZE = 100;
	/**
	 * Scan缓存的数据条数
	 */
	public static final int HBASE_SCAN_CACHE_SIZE = 1000;
	/**
	 * Scan Get默认获取的版本数
	 */
	public static final int HBASE_DEFAULT_VERSIONS = 1;

}