package com.hbase.dao;

import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.hbase.client.HTableInterface;

import com.hbase.dao.entity.HBaseRecordEntity;
import com.hbase.dao.exception.HBaseDaoException;

/**
 * HBaseDao基本方法定义
 * 
 * @author yangshenhui
 * @version 1.0
 */
public abstract class HBaseBaseDao implements HBaseDao {

	public void put(HTableInterface hTableInterface,
			List<HBaseRecordEntity> hBaseRecordEntityList)
			throws HBaseDaoException {
		put(hTableInterface, hBaseRecordEntityList, true);
	}

	public <T> void putObject(HTableInterface hTableInterface, List<T> tList)
			throws HBaseDaoException {
		putObject(hTableInterface, tList, true);
	}

	public void put(String tableName,
			List<HBaseRecordEntity> hBaseRecordEntityList)
			throws HBaseDaoException {
		put(tableName, hBaseRecordEntityList, true);
	}

	public <T> void putObject(List<T> tList) throws HBaseDaoException {
		putObject(tList, true);
	}

	public void delete(String tableName, String rowKey)
			throws HBaseDaoException {
		delete(tableName, Arrays.asList(rowKey));
	}

	protected abstract boolean merge(String tableName,
			HBaseRecordEntity hBaseRecordEntity, boolean ifInsertElseUpdate)
			throws HBaseDaoException;

	protected abstract <T> boolean mergeObject(T t, boolean ifInsertElseUpdate)
			throws HBaseDaoException;

}