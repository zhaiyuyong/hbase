package com.hbase.dao.pool;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.HTablePool;

import com.google.common.collect.Maps;
import com.hbase.dao.constant.HBaseConstant;
import com.hbase.dao.exception.HBaseDaoException;
import com.hbase.dao.util.CheckUtil;

/**
 * 管理HBase的Configuration HTablePool
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HTablePoolEngine {
	private static volatile HashMap<String, HTablePool> hTablePoolCache = Maps
			.newHashMap();
	private static volatile HashMap<String, Configuration> configurationCache = Maps
			.newHashMap();

	public static Configuration createHBaseConfiguration(
			String hBaseInstanceName, Configuration configuration)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(hBaseInstanceName);
		CheckUtil.checkNull(configuration);
		synchronized (configurationCache) {
			if (checkConfiguration(configuration)) {
				configurationCache.put(hBaseInstanceName, configuration);
			}
		}
		return configuration;
	}

	public static Configuration createHBaseConfiguration(
			String hBaseInstanceName, String hBaseZookeeperQuorum)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(hBaseInstanceName);
		CheckUtil.checkEmptyString(hBaseZookeeperQuorum);
		return createHBaseConfiguration(hBaseInstanceName,
				hBaseZookeeperQuorum,
				HBaseConstant.HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT);
	}

	public static Configuration createHBaseConfiguration(
			String hBaseInstanceName, String hBaseZookeeperQuorum,
			String hBaseZookeeperPort) throws HBaseDaoException {
		CheckUtil.checkEmptyString(hBaseInstanceName);
		CheckUtil.checkEmptyString(hBaseZookeeperQuorum);
		CheckUtil.checkEmptyString(hBaseZookeeperPort);
		Configuration configuration = HBaseConfiguration.create();
		configuration.set("hbase.zookeeper.quorum", hBaseZookeeperQuorum);
		configuration.set("hbase.zookeeper.property.clientPort",
				hBaseZookeeperPort);
		configuration.set("ipc.ping.interval", HBaseConstant.IPC_PING_INTERVAL);
		configuration.setBoolean("hbase.ipc.client.tcpnodelay",
				HBaseConstant.HBASE_IPC_CLIENT_TCPNODELAY);
		configuration.set("hbase.client.pause",
				HBaseConstant.HBASE_CLIENT_PAUSE);
		configuration.set("hbase.client.retries.number",
				HBaseConstant.HBASE_CLIENT_RETRIES_NUMBER);
		synchronized (configurationCache) {
			if (checkConfiguration(configuration)) {
				configurationCache.put(hBaseInstanceName, configuration);
			}
		}
		return configuration;
	}

	public static Configuration getHBaseConfiguration()
			throws HBaseDaoException {
		return getHBaseConfiguration(HBaseConstant.HBASE_DEFAULT_INSTANCE);
	}

	public static Configuration getHBaseConfiguration(String hBaseInstanceName)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(hBaseInstanceName);
		Configuration configuration = configurationCache.get(hBaseInstanceName);
		if ((CheckUtil.isNull(configuration))
				&& (HBaseConstant.HBASE_DEFAULT_INSTANCE
						.equals(hBaseInstanceName))) {
			return createHBaseConfiguration(
					HBaseConstant.HBASE_DEFAULT_INSTANCE,
					HBaseConstant.HBASE_ZOOKEEPER_QUORUM,
					HBaseConstant.HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT);
		}
		return configuration;
	}

	public static HTablePool getHTablePool() throws HBaseDaoException {
		Configuration configuration = getHBaseConfiguration(HBaseConstant.HBASE_DEFAULT_INSTANCE);
		if (CheckUtil.isNull(configuration)) {
			configuration = createHBaseConfiguration(
					HBaseConstant.HBASE_DEFAULT_INSTANCE,
					HBaseConstant.HBASE_ZOOKEEPER_QUORUM,
					HBaseConstant.HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT);
		}
		return getHTablePool(HBaseConstant.HBASE_DEFAULT_INSTANCE,
				HBaseConstant.HTABLE_POOL_MAX_SIZE);
	}

	public static HTablePool getHTablePool(String hBaseInstanceName, int maxSize)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(hBaseInstanceName);
		CheckUtil.checkZero(maxSize);
		HTablePool hTablePool = hTablePoolCache.get(hBaseInstanceName);
		if (CheckUtil.isNotNull(hTablePool)) {
			return hTablePool;
		}
		synchronized (hTablePoolCache) {
			hTablePool = hTablePoolCache.get(hBaseInstanceName);
			if (CheckUtil.isNotNull(hTablePool)) {
				return hTablePool;
			}
			hTablePool = new HTablePool(
					getHBaseConfiguration(hBaseInstanceName), maxSize);
			hTablePoolCache.put(hBaseInstanceName, hTablePool);
		}
		return hTablePool;
	}

	public static HTableInterface getHTable(String tableName)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(tableName);
		return getHTablePool().getTable(tableName);
	}

	public static HTableInterface getHTable(String tableName,
			String hBaseInstanceName) throws HBaseDaoException {
		CheckUtil.checkEmptyString(tableName);
		CheckUtil.checkEmptyString(hBaseInstanceName);
		return getHTablePool(hBaseInstanceName,
				HBaseConstant.HTABLE_POOL_MAX_SIZE).getTable(tableName);
	}

	public static HTableInterface createHTable(String tableName)
			throws HBaseDaoException {
		CheckUtil.checkEmptyString(tableName);
		return createHTable(tableName, HBaseConstant.HBASE_DEFAULT_INSTANCE);
	}

	public static HTableInterface createHTable(String tableName,
			String hBaseInstanceName) throws HBaseDaoException {
		CheckUtil.checkEmptyString(tableName);
		CheckUtil.checkEmptyString(hBaseInstanceName);
		try {
			return new HTable(getHBaseConfiguration(hBaseInstanceName),
					tableName);
		} catch (IOException e) {
			throw new HBaseDaoException(e);
		}
	}

	public static void closeHTable(HTableInterface hTableInterface)
			throws HBaseDaoException {
		CheckUtil.checkNull(hTableInterface);
		try {
			hTableInterface.flushCommits();
			hTableInterface.close();
		} catch (IOException e) {
			throw new HBaseDaoException(e);
		}

	}

	public static void optimizeHTable(HTableInterface hTableInterface,
			boolean autoFlush, boolean clearBufferOnFail, long writeBufferSize)
			throws HBaseDaoException {
		CheckUtil.checkNull(hTableInterface);
		CheckUtil.checkZero(writeBufferSize);
		hTableInterface.setAutoFlush(autoFlush, clearBufferOnFail);
		try {
			hTableInterface.setWriteBufferSize(writeBufferSize);
		} catch (IOException e) {
			throw new HBaseDaoException(e);
		}
	}

	public static void optimizeHTable(HTableInterface hTableInterface,
			long writeBufferSize) throws HBaseDaoException {
		CheckUtil.checkNull(hTableInterface);
		CheckUtil.checkZero(writeBufferSize);
		optimizeHTable(hTableInterface, false, false, writeBufferSize);
	}

	private static boolean checkConfiguration(Configuration configuration)
			throws HBaseDaoException {
		CheckUtil.checkNull(configuration);
		try {
			HBaseAdmin.checkHBaseAvailable(configuration);
		} catch (MasterNotRunningException | ZooKeeperConnectionException e) {
			throw new HBaseDaoException(e);
		}
		return true;
	}

}