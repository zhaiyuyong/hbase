package com.hbase.dao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * HBase列
 * 
 * @author yangshenhui
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HBaseColumn {
	/**
	 * 列族名
	 * 
	 * @return String
	 */
	public String family() default "";

	/**
	 * 列名
	 * 
	 * @return String
	 */
	public String column() default "";

}