package com.hbase.dao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * HBase表
 * 
 * @author yangshenhui
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HBaseTable {
	/**
	 * 表名
	 * 
	 * @return String
	 */
	public String tableName();

	/**
	 * 列族名
	 * 
	 * @return String
	 */
	public String family() default "";

}