package com.hbase.dao.resolve;

import java.lang.reflect.Field;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.hbase.dao.annotation.HBaseColumn;
import com.hbase.dao.annotation.HBaseRowkey;
import com.hbase.dao.annotation.HBaseTable;
import com.hbase.dao.exception.HBaseDaoException;
import com.hbase.dao.util.CheckUtil;

/**
 * HBaseTableEntityAnnotationManage
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseTableEntityAnnotationManage {
	private static volatile Map<Class<?>, HBaseTableEntityAnnotation> hBaseTableEntityAnnotationCacheMap = Maps
			.newHashMap();

	public static <T> HBaseTableEntityAnnotation getEntityAnnotation(
			Class<T> clazz) throws HBaseDaoException {
		CheckUtil.checkNull(clazz);
		HBaseTableEntityAnnotation hBaseTableEntityAnnotation = hBaseTableEntityAnnotationCacheMap
				.get(clazz);
		if (CheckUtil.isNotNull(hBaseTableEntityAnnotation)) {
			return hBaseTableEntityAnnotation;
		}
		synchronized (hBaseTableEntityAnnotationCacheMap) {
			HBaseTable hBaseTable = clazz.getAnnotation(HBaseTable.class);
			if (CheckUtil.isNull(hBaseTable)) {
				throw new HBaseDaoException(clazz.getName()
						+ " is not annotationed by @HBaseTable.");
			}
			if (StringUtils.isEmpty(hBaseTable.tableName())) {
				throw new HBaseDaoException(
						clazz.getName()
								+ " is annotationed by @HBaseTable and table name can not be empty.");
			}
			hBaseTableEntityAnnotation = new HBaseTableEntityAnnotation();
			hBaseTableEntityAnnotation.setClazz(clazz);
			for (Field field : clazz.getDeclaredFields()) {
				processHBaseRowkey(hBaseTableEntityAnnotation, field);
				processHBaseColumn(hBaseTableEntityAnnotation, field);
			}
			checkAnnotation(clazz, hBaseTableEntityAnnotation);
			hBaseTableEntityAnnotationCacheMap.put(clazz,
					hBaseTableEntityAnnotation);
		}
		return hBaseTableEntityAnnotation;
	}

	private static void processHBaseRowkey(
			HBaseTableEntityAnnotation hBaseTableEntityAnnotation, Field field)
			throws HBaseDaoException {
		CheckUtil.checkNull(hBaseTableEntityAnnotation);
		CheckUtil.checkNull(field);
		HBaseRowkey hBaseRowkey = field.getAnnotation(HBaseRowkey.class);
		if (CheckUtil.isNull(hBaseRowkey)) {
			return;
		}
		if (CheckUtil.isNotNull(hBaseTableEntityAnnotation
				.gethBaseRowkeyField())) {
			throw new HBaseDaoException(
					"@HBaseRowkey can only define on no more than one field.");
		}
		hBaseTableEntityAnnotation.sethBaseRowkeyField(field);
	}

	private static void processHBaseColumn(
			HBaseTableEntityAnnotation hBaseTableEntityAnnotation, Field field)
			throws HBaseDaoException {
		CheckUtil.checkNull(hBaseTableEntityAnnotation);
		CheckUtil.checkNull(field);
		HBaseColumn hBaseColumn = field.getAnnotation(HBaseColumn.class);
		if (CheckUtil.isNull(hBaseColumn)) {
			return;
		}
		HBaseTable hBaseTable = hBaseTableEntityAnnotation.getClazz()
				.getAnnotation(HBaseTable.class);
		checkFamily(field.getName(), hBaseColumn.family(), hBaseTable.family());
		hBaseTableEntityAnnotation.addHBaseColumn(field);
	}

	private static void checkFamily(String fieldName, String family,
			String defaultFamily) throws HBaseDaoException {
		if (StringUtils.isEmpty(family) && StringUtils.isEmpty(defaultFamily))
			throw new HBaseDaoException(fieldName
					+ " does not define a family.");
	}

	private static <T> void checkAnnotation(Class<T> clazz,
			HBaseTableEntityAnnotation hBaseTableEntityAnnotation)
			throws HBaseDaoException {
		CheckUtil.checkNull(clazz);
		CheckUtil.checkNull(hBaseTableEntityAnnotation);
		if (CheckUtil.isNull(hBaseTableEntityAnnotation.gethBaseRowkeyField())) {
			throw new HBaseDaoException(clazz.getName()
					+ " does not define a @HBaseTable field.");
		}
		if (CheckUtil.isEmpty(hBaseTableEntityAnnotation
				.gethBaseColumnFieldList()))
			throw new HBaseDaoException(clazz.getName()
					+ " should define at least one @HBaseColumn filed.");
	}

}