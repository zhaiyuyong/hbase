package com.hbase.dao.util;

/**
 * StringUtils
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class StringUtil {

	/**
	 * 首字母转换为大写
	 * 
	 * @param s
	 * @return String
	 */
	public static String initialsCapital(String s) {
		CheckUtil.checkEmptyString(s);
		int length = s.length();
		char c = s.charAt(0);
		if (Character.isUpperCase(c)) {
			return s;
		}
		return Character.toUpperCase(c) + s.substring(1, length);
	}

	private StringUtil() {
	}

}