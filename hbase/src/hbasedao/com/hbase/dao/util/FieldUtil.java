package com.hbase.dao.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.hbase.dao.exception.HBaseDaoException;

/**
 * FieldUtil
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class FieldUtil {

	public static <T> Object getFieldValue(T t, Field field)
			throws HBaseDaoException {
		CheckUtil.checkNull(t);
		CheckUtil.checkNull(field);
		try {
			String prefix = (field.getType() == Boolean.TYPE)
					|| (field.getType() == Boolean.class) ? "is" : "get";
			String methodName = prefix
					+ StringUtil.initialsCapital(field.getName());
			Method method = t.getClass().getMethod(methodName);
			return method.invoke(t, new Object[0]);
		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new HBaseDaoException(e);
		}
	}

	public static <T> void setFieldValue(T t, Field field, byte[] value)
			throws HBaseDaoException {
		CheckUtil.checkNull(t);
		CheckUtil.checkNull(field);
		if (CheckUtil.isNull(value)) {
			return;
		}
		try {
			String methodName = "set"
					+ StringUtil.initialsCapital(field.getName());
			Method method = t.getClass().getMethod(methodName, field.getType());
			method.invoke(t, TypeUtil.transform(value, field.getType()));
		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new HBaseDaoException(e);
		}
	}

	private FieldUtil() {
	}

}