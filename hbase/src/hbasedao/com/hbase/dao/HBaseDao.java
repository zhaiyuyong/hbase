package com.hbase.dao;

import java.util.List;

import org.apache.hadoop.hbase.client.HTableInterface;

import com.hbase.dao.entity.HBaseQueryEntity;
import com.hbase.dao.entity.HBaseQueryResultEntity;
import com.hbase.dao.entity.HBaseRecordEntity;
import com.hbase.dao.exception.HBaseDaoException;

/**
 * HBaseDao接口方法定义
 * 
 * @author yangshenhui
 * @version 1.0
 */
public interface HBaseDao {
	/**
	 * 插入或更新
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseRecordEntity
	 *            数据
	 * @throws HBaseDaoException
	 */
	public void put(String tableName, HBaseRecordEntity hBaseRecordEntity)
			throws HBaseDaoException;

	/**
	 * 插入或更新
	 * 
	 * @param t
	 * @throws HBaseDaoException
	 */
	public <T> void putObject(T t) throws HBaseDaoException;

	/**
	 * 批量插入或批量更新(通过调用HTable.setAutoFlush(false)方法可以将HTable写客户端的自动flush关闭
	 * 这样可以批量写入数据到HBase 而不是有一条put就执行一次更新 只有当put填满客户端写缓存时 才实际向HBase服务端发起写请求
	 * 默认情况下auto flush是开启的
	 * 通过调用HTable.setWriteBufferSize(writeBufferSize)方法可以设置HTable客户端的写buffer大小
	 * 如果新设置的buffer小于当前写buffer中的数据时 buffer将会被flush到服务端
	 * 其中writeBufferSize的单位是byte字节数 可以根据实际写入数据量的多少来设置该值)
	 * 
	 * @param hTableInterface
	 *            HBase表对象
	 * @param hBaseRecordEntityList
	 *            数据集合
	 * @param isWriteToWAL
	 *            是否写WAL日志(建议开启)
	 * @throws HBaseDaoException
	 */
	public void put(HTableInterface hTableInterface,
			List<HBaseRecordEntity> hBaseRecordEntityList, boolean isWriteToWAL)
			throws HBaseDaoException;

	/**
	 * 批量插入或批量更新
	 * 
	 * @param hTableInterface
	 *            HBase表对象
	 * @param tList
	 * @param isWriteToWAL
	 *            是否写WAL日志(建议开启)
	 * @throws HBaseDaoException
	 */
	public <T> void putObject(HTableInterface hTableInterface, List<T> tList,
			boolean isWriteToWAL) throws HBaseDaoException;

	/**
	 * 批量插入或批量更新
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseRecordEntityList
	 *            数据集合
	 * @param isWriteToWAL
	 *            是否写WAL日志(建议开启)
	 * @throws HBaseDaoException
	 */
	public void put(String tableName,
			List<HBaseRecordEntity> hBaseRecordEntityList, boolean isWriteToWAL)
			throws HBaseDaoException;

	/**
	 * 批量插入或批量更新
	 * 
	 * @param tList
	 * @param isWriteToWAL
	 *            是否写WAL日志(建议开启)
	 * @throws HBaseDaoException
	 */
	public <T> void putObject(List<T> tList, boolean isWriteToWAL)
			throws HBaseDaoException;

	/**
	 * 插入一行数据
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseRecordEntity
	 *            数据
	 * @return boolean true插入成功 false行已经存在
	 * @throws HBaseDaoException
	 */
	public boolean insert(String tableName, HBaseRecordEntity hBaseRecordEntity)
			throws HBaseDaoException;

	/**
	 * 插入一行数据
	 * 
	 * @param t
	 * @return boolean true插入成功 false行已经存在
	 * @throws HBaseDaoException
	 */
	public <T> boolean insertObject(T t) throws HBaseDaoException;

	/**
	 * 更新一行数据
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseRecordEntity
	 *            数据
	 * @return boolean true更新成功 false行不存在
	 * @throws HBaseDaoException
	 */
	public boolean update(String tableName, HBaseRecordEntity hBaseRecordEntity)
			throws HBaseDaoException;

	/**
	 * 更新一行数据
	 * 
	 * @param t
	 * @return boolean true更新成功 false行不存在
	 * @throws HBaseDaoException
	 */
	public <T> boolean updateObject(T t) throws HBaseDaoException;

	/**
	 * 查询(Scan)
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseQueryEntity
	 *            查询条件
	 * @return HBaseQueryResultEntity
	 * @throws HBaseDaoException
	 */
	public HBaseQueryResultEntity query(String tableName,
			HBaseQueryEntity hBaseQueryEntity) throws HBaseDaoException;

	/**
	 * 查询(Scan)
	 * 
	 * @param clazz
	 * @param hBaseQueryEntity
	 *            查询条件
	 * @return List<T>
	 * @throws HBaseDaoException
	 */
	public <T> List<T> queryObject(Class<T> clazz,
			HBaseQueryEntity hBaseQueryEntity) throws HBaseDaoException;

	/**
	 * 查询(Get)
	 * 
	 * @param tableName
	 *            表名
	 * @param hBaseQueryEntity
	 *            查询条件
	 * @return HBaseQueryResultEntity
	 * @throws HBaseDaoException
	 */
	public HBaseQueryResultEntity get(String tableName,
			HBaseQueryEntity hBaseQueryEntity) throws HBaseDaoException;

	/**
	 * 查询(Get)
	 * 
	 * @param clazz
	 * @param hBaseQueryEntity
	 *            查询条件
	 * @return T
	 * @throws HBaseDaoException
	 */
	public <T> T getObject(Class<T> clazz, HBaseQueryEntity hBaseQueryEntity)
			throws HBaseDaoException;

	/**
	 * 删除整行
	 * 
	 * @param tableName
	 *            表名
	 * @param rowKeyList
	 *            主键集合
	 * @throws HBaseDaoException
	 */
	public void delete(String tableName, List<String> rowKeyList)
			throws HBaseDaoException;

	/**
	 * 删除列族
	 * 
	 * @param tableName
	 *            表名
	 * @param rowKey
	 *            主键
	 * @param familys
	 *            列族数组
	 * @throws HBaseDaoException
	 */
	public void delete(String tableName, String rowKey, String... familys)
			throws HBaseDaoException;

	/**
	 * 删除列
	 * 
	 * @param tableName
	 *            表名
	 * @param rowKey
	 *            主键
	 * @param family
	 *            列族名
	 * @param columns
	 *            列名数组
	 * @throws HBaseDaoException
	 */
	public void delete(String tableName, String rowKey, String family,
			String... columns) throws HBaseDaoException;

}