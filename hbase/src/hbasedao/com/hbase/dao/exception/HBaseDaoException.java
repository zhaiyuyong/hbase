package com.hbase.dao.exception;

/**
 * HBaseDaoException
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class HBaseDaoException extends Exception {
	private static final long serialVersionUID = -4010507888347211595L;

	public HBaseDaoException() {
	}

	public HBaseDaoException(String message) {
		super(message);
	}

	public HBaseDaoException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public HBaseDaoException(Throwable throwable) {
		super(throwable);
	}

}