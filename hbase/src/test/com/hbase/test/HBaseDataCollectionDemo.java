package com.hbase.test;

import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.io.hfile.Compression.Algorithm;
import org.junit.Test;

import com.google.common.collect.Maps;
import com.hbase.dao.HBaseBaseDao;
import com.hbase.dao.HBaseDaoImpl;
import com.hbase.dao.HBaseHelper;
import com.hbase.dao.constant.HBaseConstant;
import com.hbase.dao.entity.HBaseQueryEntity;
import com.hbase.dao.entity.htable.HBaseTableDescribesEntity;
import com.hbase.dao.entity.htable.HBaseTableFamilyDescribesEntity;
import com.hbase.dao.exception.HBaseDaoException;
import com.hbase.dao.pool.HTablePoolEngine;
import com.hbase.dao.util.CheckUtil;
import com.hbase.hbasedatacollection.entity.FilePropertyEntity;

public class HBaseDataCollectionDemo {

	static {
		try {
			HTablePoolEngine.createHBaseConfiguration(
					HBaseConstant.HBASE_DEFAULT_INSTANCE, "redhat", "2181");
		} catch (HBaseDaoException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void create() throws HBaseDaoException {
		HBaseTableDescribesEntity hbDescribesEntity = new HBaseTableDescribesEntity();
		hbDescribesEntity.setTableName("fileproperty");
		String[] families = new String[] { "fp" };
		Map<String, HBaseTableFamilyDescribesEntity> familyMap = Maps
				.newHashMap();
		for (String family : families) {
			HBaseTableFamilyDescribesEntity hBaseTableFamilyDescribesEntity = new HBaseTableFamilyDescribesEntity();
			hBaseTableFamilyDescribesEntity.setCompression(Algorithm.SNAPPY);
			hBaseTableFamilyDescribesEntity
					.setCompactionCompression(Algorithm.SNAPPY);
			hBaseTableFamilyDescribesEntity.setMaxVersions(Integer.MAX_VALUE);
			familyMap.put(family, hBaseTableFamilyDescribesEntity);
		}
		hbDescribesEntity.setFamilyMap(familyMap);
		HBaseHelper.drop(hbDescribesEntity.getTableName(),
				HBaseConstant.HBASE_DEFAULT_INSTANCE);
		System.out.println(HBaseHelper.create(hbDescribesEntity,
				HBaseConstant.HBASE_DEFAULT_INSTANCE));
	}

	@Test
	public void queryObject() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseQueryEntity hBaseQueryEntity = new HBaseQueryEntity();
		hBaseQueryEntity.setCache(false);
		hBaseQueryEntity.setCacheSize(100);
		// hBaseQueryEntity.addColumn("fp", "filename");
		hBaseQueryEntity.setMaxRows(10);
		hBaseQueryEntity.setFilterOperator(Operator.MUST_PASS_ALL);
		hBaseQueryEntity.setPrefixRowKey("hbase");

		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		List<FilePropertyEntity> fList = hBaseDao.queryObject(
				FilePropertyEntity.class, hBaseQueryEntity);
		int count = 0;
		CheckUtil.checkNull(fList);
		for (FilePropertyEntity filePropertyEntity : fList) {
			++count;
			System.out.println(count + " " + filePropertyEntity);
		}
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void truncate() {
		try {
			HBaseHelper.truncate("fileproperty",
					HBaseConstant.HBASE_DEFAULT_INSTANCE);
		} catch (HBaseDaoException e) {
			e.printStackTrace();
		}
	}

}