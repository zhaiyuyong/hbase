package com.hbase.test;

import com.hbase.dao.annotation.HBaseColumn;
import com.hbase.dao.annotation.HBaseRowkey;
import com.hbase.dao.annotation.HBaseTable;

@HBaseTable(tableName = "student")
public class StudentEntity {
	@HBaseRowkey
	private String id;

	@HBaseColumn(column = "name", family = "information")
	private String name;

	@HBaseColumn(column = "age", family = "information")
	private int age;

	@HBaseColumn(column = "sex", family = "information")
	private String sex;

	@HBaseColumn(column = "address", family = "information")
	private String address;

	@HBaseColumn(column = "school", family = "other")
	private String school;

	@HBaseColumn(column = "phone", family = "other")
	private String phone;

	@HBaseColumn(column = "job", family = "other")
	private String job;

	@HBaseColumn(column = "weight", family = "other")
	private float weight;

	@HBaseColumn(column = "height", family = "other")
	private float height;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", name=" + name + ", age=" + age
				+ ", sex=" + sex + ", address=" + address + ", school="
				+ school + ", phone=" + phone + ", job=" + job + ", weight="
				+ weight + ", height=" + height + "]";
	}

}