package com.hbase.test;

import com.hbase.dao.exception.HBaseDaoException;

public class HBaseDaoClient {
	public static void main(String[] args) throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseDaoDemo hBaseDaoDemo = new HBaseDaoDemo();
		hBaseDaoDemo.create();
		hBaseDaoDemo.modify();
		for (int i = 0; i < 1000; i++) {
			hBaseDaoDemo.putObject();
		}
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}
}