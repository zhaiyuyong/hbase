package com.hbase.test;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.io.hfile.Compression.Algorithm;
import org.apache.hadoop.hbase.regionserver.StoreFile.BloomType;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hbase.dao.HBaseBaseDao;
import com.hbase.dao.HBaseDaoImpl;
import com.hbase.dao.HBaseHelper;
import com.hbase.dao.constant.HBaseConstant;
import com.hbase.dao.entity.HBaseQueryEntity;
import com.hbase.dao.entity.HBaseQueryResultEntity;
import com.hbase.dao.entity.HBaseRecordEntity;
import com.hbase.dao.entity.htable.HBaseTableDescribesEntity;
import com.hbase.dao.entity.htable.HBaseTableFamilyDescribesEntity;
import com.hbase.dao.exception.HBaseDaoException;
import com.hbase.dao.pool.HTablePoolEngine;
import com.hbase.dao.util.CheckUtil;

public class HBaseDaoDemo {
	private static Logger logger = Logger.getLogger(HBaseDaoDemo.class);
	private static String randomSource = "1234567890abcdefghijklmnopqrstuvwxyz";
	private static String tableName = "student";

	// static {
	// try {
	// HTablePoolEngine.createHBaseConfiguration(
	// HBaseConstant.HBASE_DEFAULT_INSTANCE, "redhat", "2181");
	// } catch (HBaseDaoException e) {
	// e.printStackTrace();
	// }
	// }

	@Test
	public void drop() throws HBaseDaoException {
		System.out.println(HBaseHelper.drop(tableName,
				HBaseConstant.HBASE_DEFAULT_INSTANCE));
	}

	@Test
	public void list() throws HBaseDaoException {
		for (String tableName : HBaseHelper
				.list(HBaseConstant.HBASE_DEFAULT_INSTANCE)) {
			System.out.println(tableName);
		}
	}

	@Test
	public void create() throws HBaseDaoException {
		HBaseTableDescribesEntity hbDescribesEntity = new HBaseTableDescribesEntity();
		hbDescribesEntity.setTableName(tableName);
		String[] families = new String[] { "information" };
		Map<String, HBaseTableFamilyDescribesEntity> familyMap = Maps
				.newHashMap();
		// 1G
		hbDescribesEntity.setMaxFileSize(1 * 1024 * 1024 * 1024L);
		// 256M
		hbDescribesEntity.setMemstoreFlushSize(256 * 1024 * 1024L);
		// 指定开始和结束的rowKey以及预建region的数量
		/*
		 * hbDescribesEntity.setStartRowKey(RandomStringUtils.random(10,
		 * randomNumberSource));
		 * hbDescribesEntity.setEndRowKey(RandomStringUtils.random(10,
		 * randomNumberSource)); hbDescribesEntity.setRegionNumber(5);
		 */
		// 指定切分的rowKey
		hbDescribesEntity.addSplitRowKey(RandomStringUtils.random(10,
				randomSource));
		hbDescribesEntity.addSplitRowKey(RandomStringUtils.random(10,
				randomSource));
		hbDescribesEntity.addSplitRowKey(RandomStringUtils.random(10,
				randomSource));
		hbDescribesEntity.setReadOnly(false);
		hbDescribesEntity.setDeferredLogFlush(false);
		for (String family : families) {
			HBaseTableFamilyDescribesEntity hBaseTableFamilyDescribesEntity = new HBaseTableFamilyDescribesEntity();
			hBaseTableFamilyDescribesEntity.setTimeToLive(Integer.MAX_VALUE);
			hBaseTableFamilyDescribesEntity.setBloomType(BloomType.ROW);
			hBaseTableFamilyDescribesEntity.setCompression(Algorithm.SNAPPY);
			hBaseTableFamilyDescribesEntity
					.setCompactionCompression(Algorithm.SNAPPY);
			hBaseTableFamilyDescribesEntity.setMaxVersions(1);
			hBaseTableFamilyDescribesEntity.setInMemory(false);
			// 0表示不做修改
			hBaseTableFamilyDescribesEntity.setBlockSize(0);
			hBaseTableFamilyDescribesEntity.setBlockCacheEnabled(true);
			familyMap.put(family, hBaseTableFamilyDescribesEntity);
		}
		hbDescribesEntity.setFamilyMap(familyMap);
		HBaseHelper.drop(hbDescribesEntity.getTableName(),
				HBaseConstant.HBASE_DEFAULT_INSTANCE);
		System.out.println(HBaseHelper.create(hbDescribesEntity,
				HBaseConstant.HBASE_DEFAULT_INSTANCE));
	}

	@Test
	public void modify() throws HBaseDaoException {
		HBaseTableDescribesEntity hbDescribesEntity = new HBaseTableDescribesEntity();
		hbDescribesEntity.setTableName(tableName);
		String[] families = new String[] { "information", "other" };
		Map<String, HBaseTableFamilyDescribesEntity> familyMap = Maps
				.newHashMap();
		for (String family : families) {
			HBaseTableFamilyDescribesEntity hBaseTableFamilyDescribesEntity = new HBaseTableFamilyDescribesEntity();
			hBaseTableFamilyDescribesEntity.setTimeToLive(Integer.MAX_VALUE);
			hBaseTableFamilyDescribesEntity.setBloomType(BloomType.ROW);
			hBaseTableFamilyDescribesEntity.setCompression(Algorithm.LZO);
			hBaseTableFamilyDescribesEntity
					.setCompactionCompression(Algorithm.LZO);
			hBaseTableFamilyDescribesEntity.setMaxVersions(10);
			familyMap.put(family, hBaseTableFamilyDescribesEntity);
		}
		hbDescribesEntity.setFamilyMap(familyMap);
		System.out.println(HBaseHelper.modify(hbDescribesEntity,
				HBaseConstant.HBASE_DEFAULT_INSTANCE));
	}

	@Test
	public void put() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		HTableInterface hTable = HTablePoolEngine.getHTable(tableName);
		HTablePoolEngine.optimizeHTable(hTable, 5242880L);
		List<HBaseRecordEntity> hBaseRecordEntityList = Lists.newArrayList();
		for (int i = 1; i <= 100; ++i) {
			HBaseRecordEntity hBaseRecordEntity = new HBaseRecordEntity(i + "");
			hBaseRecordEntity.addColumn("information", "name",
					RandomStringUtils.random(3, "yshwxcgwghc"));
			hBaseRecordEntity.addColumn("information", "age",
					RandomUtils.nextInt(30) + "");
			hBaseRecordEntity.addColumn("information", "sex",
					RandomStringUtils.random(1, "mf"));
			hBaseRecordEntity.addColumn("information", "address",
					RandomStringUtils.random(2, "zjjxph"));
			hBaseRecordEntity.addColumn("other", "school",
					RandomStringUtils.random(2, "dgpz"));
			hBaseRecordEntity.addColumn("other", "phone",
					RandomStringUtils.random(11, randomSource));
			hBaseRecordEntity.addColumn("other", "job",
					RandomStringUtils.random(1, "jc"));
			hBaseRecordEntity.addColumn("other", "weight",
					RandomUtils.nextInt(200) + "");
			hBaseRecordEntity.addColumn("other", "height",
					RandomUtils.nextInt(200) + "");
			hBaseRecordEntityList.add(hBaseRecordEntity);
			if (hBaseRecordEntityList.size() == 5000) {
				hBaseDao.put(hTable, hBaseRecordEntityList);
				// hBaseDao.put(tableName, hBaseRecordEntityList);
				hBaseRecordEntityList.clear();
			}
		}
		if (hBaseRecordEntityList.size() > 0) {
			hBaseDao.put(hTable, hBaseRecordEntityList);
			// hBaseDao.put(tableName, hBaseRecordEntityList);
			hBaseRecordEntityList.clear();
		}
		HTablePoolEngine.closeHTable(hTable);
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void query() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseQueryEntity hBaseQueryEntity = new HBaseQueryEntity();
		hBaseQueryEntity.setCache(false);
		hBaseQueryEntity.setCacheSize(100);
		// hBaseQueryEntity.setFilterOperator(Operator.MUST_PASS_ALL);
		hBaseQueryEntity.setMaxVersions(10);
		// hBaseQueryEntity.addColumn("information", "name");
		// hBaseQueryEntity.addColumn("information", "age");
		// hBaseQueryEntity.addFilter(new SingleColumnValueFilter(Bytes
		// .toBytes("information"), Bytes.toBytes("name"),
		// CompareOp.EQUAL, Bytes.toBytes("ysh")));
		// hBaseQueryEntity.setPrefixRowKey("5");
		// hBaseQueryEntity.setTimeStamp(1403278678033L);
		hBaseQueryEntity.setMaxRows(Integer.MAX_VALUE);
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		HBaseQueryResultEntity hBaseQueryResultEntity = hBaseDao.query(
				"fileproperty", hBaseQueryEntity);
		List<HBaseRecordEntity> hBaseRecordEntityList = hBaseQueryResultEntity
				.getRecordList(false);
		int count = 0;
		CheckUtil.checkNull(hBaseRecordEntityList);
		for (HBaseRecordEntity hBaseRecordEntity : hBaseRecordEntityList) {
			++count;
			System.out.println(count + " " + hBaseRecordEntity);
			logger.debug(hBaseRecordEntity);
		}
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void get() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseQueryEntity hBaseQueryEntity = new HBaseQueryEntity();
		hBaseQueryEntity.setCache(true);
		hBaseQueryEntity.setMaxVersions(10);
		hBaseQueryEntity.setRowKey("1");
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		HBaseQueryResultEntity hBaseQueryResultEntity = hBaseDao.get(tableName,
				hBaseQueryEntity);
		List<HBaseRecordEntity> hBaseRecordEntityList = hBaseQueryResultEntity
				.getRecordList(false);
		int count = 0;
		CheckUtil.checkNull(hBaseRecordEntityList);
		for (HBaseRecordEntity hBaseRecordEntity : hBaseRecordEntityList) {
			++count;
			System.out.println(count + " " + hBaseRecordEntity);
		}
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void truncate() throws HBaseDaoException {
		HBaseHelper.truncate(tableName, HBaseConstant.HBASE_DEFAULT_INSTANCE);
	}

	@Test
	public void putObject() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		List<StudentEntity> studentList = Lists.newArrayList();
		for (int i = 1; i <= 1000; ++i) {
			StudentEntity studentEntity = new StudentEntity();
			studentEntity.setId(RandomStringUtils.random(10, randomSource));
			studentEntity.setName(RandomStringUtils.random(3, "yshwxcgwghc"));
			studentEntity.setAge(RandomUtils.nextInt(30));
			studentEntity.setSex(RandomStringUtils.random(1, "mf"));
			studentEntity.setAddress(RandomStringUtils.random(2, "zjjxph"));
			studentEntity.setSchool(RandomStringUtils.random(2, "dgpz"));
			studentEntity.setPhone(RandomStringUtils.random(11, randomSource));
			studentEntity.setJob(RandomStringUtils.random(1, "jc"));
			studentEntity.setWeight(RandomUtils.nextInt(200));
			studentEntity.setHeight(RandomUtils.nextInt(200));
			studentList.add(studentEntity);
		}
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		hBaseDao.putObject(studentList);
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void queryObject() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseQueryEntity hBaseQueryEntity = new HBaseQueryEntity();
		hBaseQueryEntity.setCache(false);
		hBaseQueryEntity.setCacheSize(100);
		// hBaseQueryEntity.setFilterOperator(Operator.MUST_PASS_ALL);
		// hBaseQueryEntity.addColumn("information", "name");
		// hBaseQueryEntity.addFilter(new SingleColumnValueFilter(Bytes
		// .toBytes("information"), Bytes.toBytes("name"),
		// CompareOp.EQUAL, Bytes.toBytes("ysh")));
		// hBaseQueryEntity.setPrefixRowKey("5");
		// hBaseQueryEntity.setTimeStamp(1403278678033L);
		hBaseQueryEntity.setMaxRows(100);
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		List<StudentEntity> sList = hBaseDao.queryObject(StudentEntity.class,
				hBaseQueryEntity);
		int count = 0;
		CheckUtil.checkNull(sList);
		for (StudentEntity studentEntity : sList) {
			++count;
			System.out.println(count + " " + studentEntity);
		}
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void getObject() throws HBaseDaoException {
		long start = System.currentTimeMillis();
		long stop = 0L;
		HBaseQueryEntity hBaseQueryEntity = new HBaseQueryEntity();
		hBaseQueryEntity.setCache(true);
		hBaseQueryEntity.setRowKey("1");
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		StudentEntity studentEntity = hBaseDao.getObject(StudentEntity.class,
				hBaseQueryEntity);
		CheckUtil.checkNull(studentEntity);
		System.out.println(studentEntity);
		stop = System.currentTimeMillis();
		System.out.println("total cost = " + (stop - start) + " ms");
	}

	@Test
	public void updateObject() throws HBaseDaoException {
		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setId(1 + "");
		studentEntity.setName("yangshenhui");
		studentEntity.setAge(24);
		studentEntity.setSex("f");
		studentEntity.setAddress("ph");
		studentEntity.setSchool("dgpz");
		studentEntity.setPhone("18657378370");
		studentEntity.setJob("java");
		studentEntity.setWeight(70);
		studentEntity.setHeight(170);
		HBaseBaseDao hBaseDao = new HBaseDaoImpl();
		System.out.println(hBaseDao.updateObject(studentEntity));
	}

}