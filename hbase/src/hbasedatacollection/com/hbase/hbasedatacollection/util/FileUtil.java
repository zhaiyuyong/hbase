package com.hbase.hbasedatacollection.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.hbase.dao.util.CheckUtil;

import static com.google.common.collect.Lists.*;
import static com.google.common.base.Preconditions.*;

/**
 * 文件操作工具类
 * 
 * @author yangshenhui
 * @version 1.0
 */
public class FileUtil {

	public static List<String> read(String fileName) {
		checkArgument(StringUtils.isNotEmpty(fileName));
		List<String> lineList = newArrayList();
		BufferedReader bufferedReader = null;
		try {
			if (fileName.startsWith("/") && "\\".equals(File.separator)) {
				InputStream inputStream = FileUtil.class
						.getResourceAsStream(fileName);
				bufferedReader = new BufferedReader(new InputStreamReader(
						inputStream, "UTF-8"));
			} else {
				bufferedReader = new BufferedReader(new InputStreamReader(
						new FileInputStream(fileName), "UTF-8"));
			}
			String line = bufferedReader.readLine();
			while (line != null) {
				if (StringUtils.isNotEmpty(line)) {
					lineList.add(line);
				}
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return lineList;
	}

	public static void read(String fileName, List<String> lineList) {
		lineList = read(fileName);
	}

	public static void list(Collection<File> fileList, File path) {
		if (path.isFile()) {
			fileList.add(path);
		} else {
			File[] files = path.listFiles();
			if (CheckUtil.isNotEmpty(files)) {
				for (File file : files) {
					list(fileList, file);
				}
			}
		}
	}

	public static void list(Collection<File> fileList, String path) {
		list(fileList, new File(path));
	}

	public static void list(Collection<File> fileList, File path,
			Pattern pattern) {
		if (path.isFile()) {
			if (pattern.matcher(path.getName()).matches()) {
				fileList.add(path);
			}
		} else {
			File[] files = path.listFiles();
			if (CheckUtil.isNotEmpty(files)) {
				for (File file : files) {
					list(fileList, file, pattern);
				}
			}
		}
	}

	public static void list(Collection<File> fileList, String path, String regex) {
		if (StringUtils.isEmpty(regex)) {
			list(fileList, path);
		} else {
			list(fileList, new File(path), Pattern.compile(regex));
		}
	}

	public static void move(String sourceFileName, String destFileName) {
		File sourceFile = new File(sourceFileName);
		File destFile = new File(destFileName);
		sourceFile.renameTo(destFile);
	}

	public static boolean exists(String fileNameOrPath, boolean isCreate) {
		File file = new File(fileNameOrPath);
		if (file.exists()) {
			return true;
		} else {
			if (isCreate) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return false;
		}
	}

}