package com.hbase.hbasedatacollection.entity;

import java.util.Date;

import com.hbase.dao.annotation.HBaseColumn;
import com.hbase.dao.annotation.HBaseRowkey;
import com.hbase.dao.annotation.HBaseTable;

@HBaseTable(tableName = "fileproperty")
public class FilePropertyEntity {
	@HBaseRowkey
	private String id;

	@HBaseColumn(column = "filename", family = "fp")
	private String fileName;

	@HBaseColumn(column = "fullpath", family = "fp")
	private String fullPath;

	@HBaseColumn(column = "lastmodified", family = "fp")
	private Date lastModified;

	@HBaseColumn(column = "filesize", family = "fp")
	private long fileSize;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	@Override
	public String toString() {
		return "FilePropertyEntity [id=" + id + ", fileName=" + fileName
				+ ", fullPath=" + fullPath + ", lastModified=" + lastModified
				+ ", fileSize=" + fileSize + "]";
	}

}